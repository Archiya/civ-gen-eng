'''
///////////////////////////////////////////////////////////////////////////////
BIOME EQUATIONS V.01 : CG
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
'''
'''============================================================================
Define the line that dictates how biomes get categorized.
|                              / TRPCLRNFRST
|                       /
|                /  The rest fall under here
|         /         somewhere based on the
|  / TUNDRA         slope given below.
_______________________________________________
============================================================================'''
def Tundra(x):               return (x*2)  + 0
def BorealForest(x):         return (x*10) + 100
def TemperateGrassland(x):   return (x*0)  + 50
def Woodland(x):             return (x*0)  + 100
def TemperateForest(x):      return (x*0)  + 220  
def TemperateRainforest(x):  return (x*10) + 220
def SubtropicalDesert(x):    return (x*0)  + 50
def Savannah(x):             return (x*0)  + 220
def TropicalRainforest(x):   return (x*10) + 320
'''
===============================================================================
BiomeDeterminer:
    Given the above set of functions and a set of bounds, determine what a
    given set of (x, y) coordinates are.
===============================================================================
'''
class BiomeDeterminer:
    def __init__(self):
        self.biome_funcs = [
            Tundra,
            BorealForest,
            TemperateGrassland,
            Woodland,
            TemperateForest,
            TemperateRainforest,
            SubtropicalDesert,
            Savannah,
            TropicalRainforest
        ]
        self.biome_y_bounds = [
            (-50, 0),    #Tundra
            (0  , 8),    #BorealForest
            (0,   20),   #TemperateGrassland
            (8,   20),   #Woodland
            (8,   20),   #TemperateForest
            (12,  20),   #TemperateRainforest
            (20,  30),   #SubtropicalDesert
            (20,  30),   #Savannah
            (20,  30)    #TropicalRainforest
        ]
        self.biome_x_bounds = [
            (0,   100),  #Tundra
            (50,  200),  #BorealForest
            (0,   50),   #TemperateGrassland
            (50,  100),  #Woodland
            (100, 220),  #TemperateForest
            (220, 320),  #TemperateRainforest
            (0,   50),   #SubtropicalDesert
            (50,  220),  #Savannah
            (220, 400)   #TropicalRainforest
        ]

    def IsBiome(self, func, x, y, bound_x, bound_y): 
        y_2 = func(x)
        return y_2 >= y and \
               y   >=  bound_y[0] and \
               y   <=  bound_y[1] and \
               x   >=  bound_x[0] and \
               x   <=  bound_x[1] 

    def DetermineBiome(self, x, y):
        for i, func in enumerate(self.biome_funcs):
            if (self.IsBiome(
                    func, 
                    x, 
                    y, 
                    self.biome_x_bounds[i], 
                    self.biome_y_bounds[i]
                )):
                return func.__name__