import numpy as np
import math
import random
import time
from noise import pnoise2
from .grid_config import temps

class PerlinGridGenerator:
    def __init__(self):
        pass
    def GenerateBaseTile(
        self, 
        shape = (16, 16), 
        scale = 7, 
        octaves = 7, 
        persistence = .5, 
        lacunarity = 2.0,
        seed = None
    ):
        if not seed:
            seed = np.random.randint(0, 100)
            print("seed was {}".format(seed))

        arr = np.zeros(shape)
        for i in range(shape[0]):
            for j in range(shape[1]):
                arr[i][j] = pnoise2(i / scale,
                                    j / scale,
                                    octaves=octaves,
                                    persistence=persistence,
                                    lacunarity=lacunarity,
                                    repeatx=1024,
                                    repeaty=1024,
                                    base=seed
                                    )
        max_arr = np.max(arr)
        min_arr = np.min(arr)
        norm_me = lambda x: (x-min_arr)/(max_arr - min_arr)
        norm_me = np.vectorize(norm_me)
        arr = norm_me(arr)
        self.grid = arr
        return arr
    def ToString(self):
        return self.grid.tostring()

class WorldTilesGenerator(PerlinGridGenerator):
    def __init__(self, variance_step):
        self.tiles = None
        self.variance_step = variance_step
    def GenerateWaterTiles(
        self,
        size=4, 
        shape=(16,16), 
        percentage_water=.5
    ):
        self.GenerateBaseTile(shape)
        num_water_tiles = round((size[0]*size[1]) * percentage_water)
        #get the total mean of all tiles of perlin noise
        average_val = np.percentile(
            np.percentile(self.grid, (percentage_water*100), axis=1), 
            (percentage_water*100), 
            axis=0
        )

        #Go from perlin noise to a tileset.
        self.water_grid = np.zeros(shape)
        for ix,iy in np.ndindex(self.grid.shape):
            if (num_water_tiles > 0):
                if (self.grid[ix, iy] - average_val) < 0: 
                    num_water_tiles -= 1
                    self.water_grid[ix,iy] = 1
            else:
                break
        return self.water_grid
    
    def GenerateTemperatureGradientTiles(
        self,
        temperature,
        shape=(16,16),
        size=4,
        variance=1
    ):
        self.temp_grid = np.zeros(shape)
        a = shape[0]
        b = temperature
        mid = (shape[0] / 2)

        for idx, x in np.ndenumerate(self.temp_grid):
            try:
                temp_at_point = (b/a)*(math.sqrt(a**2-((idx[0]+1) - mid)**2))
            except ValueError as e:
                temp_at_point = -(b/a)*(math.sqrt(a**2-((idx[0]+1) - mid)**2))
            self.temp_grid[idx] = temp_at_point
            print(temp_at_point)
        return self.temp_grid
        # self.temp_grid = np.zeros(shape)
        # mid_point = (size/2) - 1
        # for idx, x in np.ndenumerate(self.temp_grid):
        #     temp_at_point = temperature + ((mid_point-idx[0]) * variance * (-1 if (idx[0]<mid_point) else 1) )
        #     self.temp_grid[idx] = temp_at_point
        # return self.temp_grid

    def GenerateBiomeTiles(
        self,
        biome_list,
        temperature,
        shape=(16,16),
        size=4
    ):
        self.biome_grid = np.zeros(shape)
        #Iterate over the temperature grid and match temperatures.
        for ix,iy in np.ndindex(self.temp_grid.shape):
            #random.shuffle(biome_list)
            best_fit = 1000
            biome_selection = None
            for biome in biome_list:
                #Grab the two values to compare.
                biome_temp_to_check = (temps[biome])[0]
                grid_temp_to_check = self.temp_grid[ix, iy]

                if (biome_temp_to_check < 0 and grid_temp_to_check > 0):
                    continue
                if (grid_temp_to_check < 0 and biome_temp_to_check > 0):
                    continue
                #Get the absolute value so nothing funky happens with subtraction.
                if (biome_temp_to_check < 0): 
                    biome_temp_to_check = abs(biome_temp_to_check)
                if (grid_temp_to_check < 0):
                    grid_temp_to_check = abs(grid_temp_to_check)
                
                if (abs(biome_temp_to_check - grid_temp_to_check) < best_fit):
                    best_fit = abs(biome_temp_to_check - grid_temp_to_check)
                    biome_selection = (temps[biome])[1]
            self.biome_grid[ix, iy] = biome_selection
        return self.biome_grid

    def CombineWaterAndBiomeGrid(self):
        self.tiles = np.zeros(self.biome_grid.shape)
        for ix,iy in np.ndindex(self.water_grid.shape):
            #If the tile is land:
            if (self.water_grid[ix, iy] == 0):
                self.tiles[ix, iy] = self.biome_grid[ix, iy]
            if (self.water_grid[ix, iy] == 1):
                self.tiles[ix, iy] = 0
        return self.tiles

    def GenerateSubMap(self, shape):
        self.submap = np.zeros(shape)
        now = time.time()
        for ir,ic in np.ndindex(self.submap.shape[:2]):
            for ix, iy in np.ndindex(self.submap[ir, ic].shape):
                self.submap[ir, ic, ix, iy] = self.grid[ix, iy]
        end = time.time()
        ellapsed = end - now
        print(ellapsed)        

            

    #def GenerateSubTiles(self, size, biome):
        


                
                







