import pygame
import yaml
import sys
import os
from sprite import Sprite
#-------------------------------------------------------------
'''
Map:
    Handles the creation of the Map. Currently very empty, 
    but will contain more as features are implemented.
'''
class Tile(Sprite): 
    def __init__(self, image):
        Sprite.__init__(self, "tileset.yml", image)
