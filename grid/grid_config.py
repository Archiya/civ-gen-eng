#EXPECTED TEMPERATURES
temps = {
    'Tundra': (250, 1),
    'BorealForest': (260, 2),
    'TemperateGrassland': (270, 3),
    'Woodland': (275, 4),
    'TemperateForest': (280, 5),
    'TemperateRainforest':(283, 6),
    'SubtropicalDesert': (286, 7),
    'Savannah': (289, 8),
    'TropicalRainforest': (292, 9)
}

#EXPECTED TEMPERATURES
int_to_biome = {
    0 : 'Ocean',
    1 : 'Tundra',
    2 : 'BorealForest',
    3 : 'TemperateGrassland',
    4 : 'Woodland',
    5 : 'TemperateForest',
    6 : 'TemperateRainforest',
    7 : 'SubtropicalDesert',
    8 : 'Savannah',
    9 : 'TropicalRainforest'
}

temp_step = 2