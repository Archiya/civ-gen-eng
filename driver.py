# Classes ---------------------------------------------------------------------
from World import World
from tile import Tile
from tilemanager import TileManager
from MoveableImage import MoveableImage
#Libs     ---------------------------------------------------------------------
import numpy as np
import pygame
import sys
import yaml
import os
#Vars     ---------------------------------------------------------------------
with open(os.path.join("dat", 'config.yml')) as file: cf = yaml.full_load(file)
dir_path = os.path.dirname(os.path.realpath('dat/config.py'))

class Game:
    '''------------------------------------------------------------------------
    Init:
        Defines initial operations necessary for pygame to run.
        Defines World object necessary for tile generation.
        Defines TileManager object responsible for display tiles.
    ------------------------------------------------------------------------'''
    def __init__(self):
        pygame.init()
        pygame.display.set_caption(cf['TITLE'])
        #pg.key.set_repeat(500, 100)
        self.clock       = pygame.time.Clock()
        self.fps         = cf['FPS']
        screen_width     = cf['SCREEN_WIDTH']
        screen_height    = cf['SCREEN_HEIGHT']
        self.tile_size   = cf['TILE_SIZE']
        self.tile_size_x = cf['WORLD_SIZE_X']
        self.tile_size_y = cf['WORLD_SIZE_Y']
        self.sub_size_x  = cf['SUBWORLD_SIZE_X']
        self.sub_size_y  = cf['SUBWORLD_SIZE_Y']
        self.screen    = pygame.display.set_mode(
                         (self.tile_size*screen_width, 
                          self.tile_size*screen_height)
        )
        self.world = World(
            (self.tile_size_x, self.tile_size_y), 
            (self.sub_size_x, self.sub_size_y),
            cf['AVG_RAIN'], 
            cf['MAX_TEMP']
        )
        self.tm = TileManager(
            self.world, 
            (self.tile_size), 
            screen_width, 
            screen_height
        )
        self.selection = MoveableImage(
            'WhiteSquare',
            self.tile_size,
            (0,0)
        )
    '''------------------------------------------------------------------------
    Events:
        KEYDOWN:
            ESCAPE: QUIT
    ------------------------------------------------------------------------'''
    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.tm.AddViewportOffset((0,-1))
                    self.selection.UpdateOffscreenMovement((0,-1))
                elif event.key == pygame.K_DOWN:
                    self.tm.AddViewportOffset((0,1))
                    self.selection.UpdateOffscreenMovement((0,1))
                elif event.key == pygame.K_LEFT:
                    self.tm.AddViewportOffset((-1,0))
                    self.selection.UpdateOffscreenMovement((-1,0))
                elif event.key == pygame.K_RIGHT:
                    self.tm.AddViewportOffset((1,0))
                    self.selection.UpdateOffscreenMovement((1,0))
                elif event.key == pygame.K_w:
                    self.selection.move((0,-1))
                elif event.key == pygame.K_s:
                    self.selection.move((0,1))
                elif event.key == pygame.K_a:
                    self.selection.move((-1,0))
                elif event.key == pygame.K_d:
                    self.selection.move((1,0))
                elif event.key == pygame.K_RETURN:
                    print(self.selection.GetCurrentPos())
                elif event.key == pygame.K_ESCAPE:
                    self.quit()
    '''------------------------------------------------------------------------
    Draw:
        Draw the Tile Manager's tileset.
    ------------------------------------------------------------------------'''
    def draw(self):
        self.tm.draw(self.screen)
        self.selection.draw(self.screen)
        pygame.display.flip()
    '''------------------------------------------------------------------------
    Update:
        Draw the Tile Manager's tileset.
    ------------------------------------------------------------------------'''
    def update(self):
        self.tm.update()
    '''------------------------------------------------------------------------
    Quit:
        Quit out of pygame, exit to system.
    ------------------------------------------------------------------------'''    
    def quit(self):
        pygame.quit()
        sys.exit()
    '''------------------------------------------------------------------------
    Run:
        Process events.
        Update the tilemanager, which updates each tile.
        Draw tiles.
    ------------------------------------------------------------------------'''
    def run(self):
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(self.fps) / 1000
            self.events()
            self.update()
            self.draw()
#Main     ---------------------------------------------------------------------
if __name__ == '__main__':
    app = Game()
    app.run()
