import pygame
import numpy as np
import yaml
import sys
import os
from tile import Tile
from grid.grid_config import int_to_biome

class TileManager():
    def __init__(self, world, tile_offset, screen_width, screen_height):
        self.grid = world.grid
        self.size = (self.grid.shape)
        self.tile_offset = tile_offset
        self.screen_width = screen_width
        self.screen_height = screen_height
        #Defines where we draw from.
        self.origin = (0,0)
        #Initialize double array. This is being used instead of a 
        self.tile_map = [[0 for x in range(self.size[1])] for y in range(self.size[0])]
        for ix,iy in np.ndindex((self.grid.shape)):
            active_tile = int_to_biome[self.grid[ix, iy]]
            self.tile_map[ix][iy] = Tile(active_tile)

    def rotated(self, array_2d):
        list_of_tuples = zip(*array_2d[::-1])
        return [list(elem) for elem in list_of_tuples]
    
    def draw(self, screen):
        rotated_tile_map = self.rotated(self.tile_map)
        for x in range(self.screen_width):
            for y in range(self.screen_height):
                x_offset = self.tile_offset*(x)
                y_offset = self.tile_offset*(y)
                rotated_tile_map[x+self.origin[0]][y+self.origin[1]].draw(screen, (x_offset, y_offset))

    def AddViewportOffset(self, movement):
        new_origin = ((self.origin[0] + movement[0]), (self.origin[1] + movement[1]))
        self.origin = new_origin

    def update(self):
        return True
