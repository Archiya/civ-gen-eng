#EXPECTED TEMPERATURES
temps = {
    'Tundra': (-25, 0),
    'BorealForest': (4, 1),
    'TemperateGrassland': (10, 2),
    'Woodland': (14, 3),
    'TemperateForest': (14, 4),
    'TemperateRainforest': (16, 5),
    'SubtropicalDesert': (25, 6),
    'Savannah': (25, 7),
    'TropicalRainforest': (25, 8)
}

int_to_biome = {
    0 : 'Ocean',
    1 : 'Tundra',
    2 : 'BorealForest',
    3 : 'TemperateGrassland',
    4 : 'Woodland',
    5 : 'TemperateForest',
    6 : 'TemperateRainforest',
    7 : 'SubtropicalDesert',
    8 : 'Savannah',
    9 : 'TropicalRainforest'
}


temp_step = 2