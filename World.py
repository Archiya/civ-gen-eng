from grid.Grid          import WorldTilesGenerator
from grid.grid_config import *

class World():
    def __init__(self, overworld_size, subworld_size, rain, temp):
        #Total number of occupiable tiles
        self.total_biome_amount = overworld_size
        self.sub_biome_amount   = subworld_size 
        self.biome_list = [
            'Tundra',
            'BorealForest',
            'TemperateGrassland',
            'Woodland',
            'TemperateForest',
            'TemperateRainforest',
            'SubtropicalDesert',
            'Savannah',
            'TropicalRainforest'
        ]
        self.perlin_gen = WorldTilesGenerator(temp_step)
        self.water_grid = self.perlin_gen.GenerateWaterTiles(
            size=self.total_biome_amount,
            shape=(self.total_biome_amount),
            percentage_water=.6
        )
        self.temp_grid  = self.perlin_gen.GenerateTemperatureGradientTiles(
            temperature=temp,
            shape=(self.total_biome_amount),
            size=self.total_biome_amount,
            variance=temp_step
        )
        self.biome_grid = self.perlin_gen.GenerateBiomeTiles(
            biome_list=self.biome_list,
            temperature=temp,
            shape=(self.total_biome_amount),
            size=self.total_biome_amount
        )
        self.grid = self.perlin_gen.CombineWaterAndBiomeGrid()
        self.sub_biome_grid = self.perlin_gen.GenerateSubMap(
            shape=(self.total_biome_amount[0], 
                   self.total_biome_amount[1],
                   self.sub_biome_amount[0], 
                   self.sub_biome_amount[1]
            )
            
        )