'''
///////////////////////////////////////////////////////////////////////////////
BIOME V.01 : CG
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
'''
# CLASSES----------------------------------------------------------------------
from .biome_equations_config import BiomeDeterminer
from .biome_config import *
# LIBS-------------------------------------------------------------------------
import random
'''
===============================================================================
BiomeGenerator:
    Given the size of the planet to be generated, generates the correct number
    of biomes based on the configurable average rainfall, average temperature,
    rainfall variance, and temperature variance.
===============================================================================
'''
class BiomeGenerator:

    '''------------------------------------------------------------------------
    INIT
    pre:        size > 0 && size < 4
    post:       determiner != NULL
    ------------------------------------------------------------------------'''
    def __init__(self, size):
        self.size = size
        self.num_biomes = size
        self.rainfall_variance = 100
        self.temperature_variance = (temp_step*((size/2)-1))
        self.determiner = BiomeDeterminer()
        self.biome_list = []
    '''------------------------------------------------------------------------
    GENERATE
    pre:        y = m (average_rainfall) + b == (temperature != None) 
                (average_temperature != None) = mx + b == (rainfall != None)
    post:       biome_list != NULL
    ------------------------------------------------------------------------'''
    def Generate(self, average_rainfall, average_temperature):
        for i in range(16):
            valid_biome = False
            biome = None
            #Very inefficent. Shuffle the values til they fall under the curve.
            while not valid_biome:
                #rain = average_rainfall +- random(average_rainfall)
                rain = (random.randint(
                            -self.rainfall_variance, self.rainfall_variance
                        )) + average_rainfall
                #temp = average_temperate +- random(average_temperature)
                temp = (random.randint(
                            -self.temperature_variance, self.temperature_variance
                        )) + average_temperature
                biome = self.determiner.DetermineBiome(rain, temp)
                valid_biome = True if (biome is not None) else False  
            self.biome_list.append(biome)
        return self.biome_list