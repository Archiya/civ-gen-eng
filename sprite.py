import pygame
import os
import yaml
#-------------------------------------------------------------
'''
Sprite:
    Handles drawing of anything that needs to be a sprite.
    Exists as a base class to be inherited from for basic
    drawing capabilities.
'''
class Sprite:
    #Init
    def __init__(self, yaml_doc, image):
        with open(os.path.join("dat", yaml_doc)) as file: map = yaml.full_load(file)
        self.frame_buffer = []
        for frame in map[image]:
            current_frame = pygame.image.load(os.path.join(map["directory"], frame))
            self.frame_buffer.append(current_frame)
        self.current_frame = 0
    #Draw
    def draw(self, screen, location):
        screen.blit(self.frame_buffer[self.current_frame % len(self.frame_buffer)], location)
        self.current_frame = self.current_frame + 1

