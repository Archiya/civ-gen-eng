import pygame
import yaml
import sys
import os
from sprite import Sprite

class MoveableImage(Sprite): 
    def __init__(self, image, tile_offset, origin):
        #TODO: change this eventually to be more generic.
        Sprite.__init__(self, "tileset.yml", image)
        self.origin = origin
        self.offscreen_movement = (0,0)
        self.tile_offset = tile_offset

    def UpdateOffscreenMovement(self, implicit_movement):
        new_offscreen = ((self.offscreen_movement[0] + implicit_movement[0]), (self.offscreen_movement[1] + implicit_movement[1]))
        self.offscreen_movement = new_offscreen

    def move(self, movement):
        new_origin = ((self.origin[0] + movement[0]), (self.origin[1] + movement[1]))
        self.origin = new_origin

    def draw(self, screen):
        x_offset = self.origin[0] * self.tile_offset
        y_offset = self.origin[1] * self.tile_offset
        Sprite.draw(self, screen, (x_offset, y_offset))
    
    def GetCurrentPos(self):
        movement = ((self.origin[0] + self.offscreen_movement[0]), ((self.origin[1] + self.offscreen_movement[1])))
        return movement
